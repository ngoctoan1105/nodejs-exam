const express = require("express");
const router = require("express-promise-router")();

const BookController = require("../controllers/book");

router.route("/").get(BookController.GetAll).post(BookController.Insert);
router
  .route("/:id")
  .get(BookController.GetById)
  .put(BookController.Update)
  .delete(BookController.Delete);

module.exports = router;

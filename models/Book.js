const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const BookSchema = new Schema({
  image: {
    type: String,
  },
  name: {
    type: String,
  },
  price: {
    type: Number,
  },
  cover: {
    type: String,
  },
  author: {
    type: String,
  },
  stock: {
    type: Number,
  },
  date_publish: {
    type: Date,
  },
  publisher: {
    type: String,
  },
  supplier: {
    type: String,
  },
});

const Book = mongoose.model("Book", BookSchema);
module.exports = Book;

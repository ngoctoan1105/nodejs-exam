const Book = require("../models/Book");
const Deck = require("../models/Deck");

const GetAll = async (req, res, next) => {
  const books = await Book.find({});
  return res.status(200).json({ books });
};
const GetById = async (req, res, next) => {
  const book = await Book.findById(req.params.id);
  return res.status(200).json({ book });
};
const Insert = async (req, res, next) => {
  const newBook = new Book(req.body);
  await newBook.save();
  return res.status(201).json({ book: newBook });
};
const Update = async (req, res, next) => {
  const { id } = req.params;
  const updateBook = req.body;
  const result = await Book.findByIdAndUpdate(id, updateBook);
  return res.status(200).json({ success: true });
};
const Delete = async (req, res, next) => {
    const {id} = req.params;
    const book = await Book.findByIdAndDelete(id);
    return res.status(200).json({ success: true, book})
}
module.exports = {
  GetAll,
  GetById,
  Insert,
  Update,
  Delete
};
